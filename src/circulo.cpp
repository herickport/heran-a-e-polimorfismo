#include "circulo.hpp"

#include <math.h>

Circulo::Circulo() {
    set_tipo("Círculo");
    set_raio(1.0f);
}

Circulo::Circulo(float raio) {
    set_tipo("Círculo");
    set_raio(raio);
}

Circulo::~Circulo() {
    
}

float Circulo::get_raio() {
    return raio;
}

void Circulo::set_raio(float raio) {
    if(raio < 0)
        throw(1);
    else
        this->raio = raio;
}

float Circulo::calcula_area() {
    return M_PI*pow(get_raio(), 2);
}

float Circulo::calcula_perimetro() {
    return 2*M_PI*get_raio();
}
