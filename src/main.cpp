#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "circulo.hpp"
#include "hexagono.hpp"
#include "triangulo.hpp"
#include "trianguloequilatero.hpp"
#include "trianguloretangulo.hpp"

#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int main(int argc, char ** argv) {

	vector<FormaGeometrica *> formasGeometricas;

	formasGeometricas.push_back(new Quadrado());
	formasGeometricas.push_back(new Paralelogramo());
	formasGeometricas.push_back(new Pentagono());
	formasGeometricas.push_back(new Circulo());
	formasGeometricas.push_back(new Hexagono());
	formasGeometricas.push_back(new TrianguloEquilatero());
	formasGeometricas.push_back(new TrianguloRetangulo());

	formasGeometricas.push_back(new Quadrado(5.0, 10.0));
	formasGeometricas.push_back(new Paralelogramo(5.0, 10.0, 60.0));
	formasGeometricas.push_back(new Pentagono(5.0));
	formasGeometricas.push_back(new Circulo(5.0));
	formasGeometricas.push_back(new Hexagono(5.0));
	formasGeometricas.push_back(new TrianguloEquilatero(5.0));
	formasGeometricas.push_back(new TrianguloRetangulo(5.0, 10.0));

	cout << setprecision(2) << fixed;

	cout << "\n======================= Lista de Formas Geométricas =======================\n\n";
	for (FormaGeometrica * figura : formasGeometricas) {
		cout << "  Forma Geométrica: " << figura->get_tipo() << ", ";
		cout << "Área: " << figura->calcula_area() << ", ";
		cout << "Perímetro: " << figura->calcula_perimetro() << ".\n";
		cout << " =========================================================================\n";
	}

	cout << endl;

	return 0;
}