#include "hexagono.hpp"

#include <math.h>

Hexagono::Hexagono() {
    set_tipo("Hexágono");
    set_base(1.0f);
}

Hexagono::Hexagono(float base) {
    set_tipo("Hexágono");
    set_base(base);
}

Hexagono::~Hexagono() {

}

float Hexagono::get_apotema() {
    apotema = get_base()/(2*tan(30*M_PI/180));
    return apotema;
}

float Hexagono::calcula_area() {
    return 6*(get_base()*get_apotema()/2);
}

float Hexagono::calcula_perimetro() {
    return 6*get_base();
}
