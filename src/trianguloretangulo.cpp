#include "trianguloretangulo.hpp"

#include <math.h>

TrianguloRetangulo::TrianguloRetangulo() {
    set_tipo("Triângulo Retângulo");
    set_base(1.0f);
    set_altura(1.0f);
}

TrianguloRetangulo::TrianguloRetangulo(float base, float altura) {
    set_tipo("Triângulo Retângulo");
    set_base(base);
    set_altura(altura);
}

TrianguloRetangulo::~TrianguloRetangulo() {

}

float TrianguloRetangulo::calcula_perimetro() {
    return get_base() + get_altura() + sqrt(pow(get_base(), 2) + pow(get_altura(), 2));
}
