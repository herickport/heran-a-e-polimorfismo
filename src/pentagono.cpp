#include "pentagono.hpp"

#include <math.h>

Pentagono::Pentagono() {
    set_tipo("Pentágono");
    set_base(1.0f);
}

Pentagono::Pentagono(float base) {
    set_tipo("Pentágono");
    set_base(base);
}

Pentagono::~Pentagono() {

}

float Pentagono::get_apotema() {
    apotema = get_base()/(2*tan(36*M_PI/180));
    return apotema;
}

float Pentagono::calcula_area() {
    return 5*(get_base()*get_apotema()/2);
}

float Pentagono::calcula_perimetro() {
    return 5*get_base();
}
