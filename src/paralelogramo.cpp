#include "paralelogramo.hpp"

#include <math.h>

Paralelogramo::Paralelogramo() {
    set_tipo("Paralelogramo");
    set_base(1.0f);
    set_altura(1.0f);
    set_angulo(45.0f);
}

Paralelogramo::Paralelogramo(float base, float altura) {
    set_tipo("Paralelogramo");
    set_base(base);
    set_altura(altura);
    set_angulo(45.0f);
}

Paralelogramo::Paralelogramo(float base, float altura, float angulo) {
    set_tipo("Paralelogramo");
    set_base(base);
    set_altura(altura);
    set_angulo(angulo);
}

Paralelogramo::~Paralelogramo() {

}

float Paralelogramo::get_angulo() {
    return angulo;
}

void Paralelogramo::set_angulo(float angulo) {
    if(angulo < 0)
        throw(1);
    else
        this->angulo = angulo;
}

float Paralelogramo::calcula_perimetro() {
    return 2*get_base() + 2*(get_altura()/sin(angulo*M_PI/180)); 
}
