#include "triangulo.hpp"

Triangulo::Triangulo() {
    set_tipo("Triângulo");
    set_base(1.0f);
    set_altura(1.0f);
}

Triangulo::Triangulo(float base, float altura) {
    set_tipo("Triângulo");
    set_base(base);
    set_altura(altura);
}

Triangulo::~Triangulo() {

}

float Triangulo::calcula_area() {
    return get_base()*get_altura()/2;
}

float Triangulo::calcula_perimetro() {
    return 3*get_base();
}
