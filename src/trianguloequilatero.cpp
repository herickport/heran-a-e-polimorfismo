#include "trianguloequilatero.hpp"

#include <math.h>

TrianguloEquilatero::TrianguloEquilatero() {
    set_tipo("Triângulo Equilátero");
    set_base(1.0f);
}

TrianguloEquilatero::TrianguloEquilatero(float base) {
    set_tipo("Triângulo Equilátero");
    set_base(base);
}

TrianguloEquilatero::~TrianguloEquilatero() {

}

float TrianguloEquilatero::calcula_area() {
    return pow(get_base(), 2) * sqrt(3)/4;
}
