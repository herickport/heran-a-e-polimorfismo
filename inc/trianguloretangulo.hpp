#ifndef RETANGULO_HPP
#define RETANGULO_HPP

#include "triangulo.hpp"

class TrianguloRetangulo : public Triangulo {
    public:
        TrianguloRetangulo();
        TrianguloRetangulo(float base, float altura);
        ~TrianguloRetangulo();
        float calcula_perimetro();
};

#endif