#ifndef EQUILATERO_HPP
#define EQUILATERO_HPP

#include "triangulo.hpp"

class TrianguloEquilatero : public Triangulo {
    public:
        TrianguloEquilatero();
        TrianguloEquilatero(float base);
        ~TrianguloEquilatero();
        float calcula_area();
};

#endif