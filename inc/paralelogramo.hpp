#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"

class Paralelogramo : public FormaGeometrica {
    private:
        float angulo;
    public:
        Paralelogramo();
        Paralelogramo(float base, float altura);
        Paralelogramo(float base, float altura, float angulo);
        ~Paralelogramo();
        float get_angulo();
        void set_angulo(float angulo);
        float calcula_perimetro();
};

#endif