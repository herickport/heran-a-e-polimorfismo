#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "formageometrica.hpp"

class Hexagono : public FormaGeometrica {
    private:
        float apotema;
    public:
        Hexagono();
        Hexagono(float base);
        ~Hexagono();
        float get_apotema();
        float calcula_area();
        float calcula_perimetro();
};

#endif