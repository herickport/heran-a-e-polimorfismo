#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"

class Circulo : public FormaGeometrica {
    private:
        float raio;
    public:
        Circulo();
        Circulo(float raio);
        ~Circulo();
        float get_raio();
        void set_raio(float raio);
        float calcula_perimetro();
        float calcula_area();
};

#endif