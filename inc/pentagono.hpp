#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"

class Pentagono : public FormaGeometrica {
    private:
        float apotema;
    public:
        Pentagono();
        Pentagono(float base);
        ~Pentagono();
        float get_apotema();
        float calcula_area(); 
        float calcula_perimetro();
};

#endif